import './css/index.scss';
import React from 'react';
import ReactDOM from 'react-dom/client';
import RulloGame from "./RulloGame";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <RulloGame />
  </React.StrictMode>
);