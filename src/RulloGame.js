import './css/index.scss';

import React from "react";
import GameGenerator from "./Engine/GameGenerator";
import AvailableGameRules from "./Engine/AvailableGameRules";
import Board from "./Engine/Board";

/**
 * The main game orchestrator
 */
class RulloGame extends React.Component {
    constructor(props) {
        super(props);

        this.availableGameRules = AvailableGameRules();

        this.gameStages = {
            'selectRange': 0,
            'selectSize': 1,
            'startGame': 2
        }

        this.gameModes = {
            'easy':   '0-9',
            'medium': '0-19',
            'hard':   '2-4',
        }

        this.gameSizes = {
            '5x5': '5x5',
            '6x6': '6x6',
            '7x7': '7x7',
            '8x8': '8x8',
        };

        this.gameRange = null;
        this.gameSize = null;
        this.gameRule = null;

        this.state = {
            // Commands the current screen
            stage: 0,
        };
    }

    isScreen(expectedScreen) {
        return (this.state.stage === this.gameStages[expectedScreen]);
    }

    prepareGame() {
        this.availableGameRules.setRange(this.gameRange);
        this.availableGameRules.setSize(this.gameSize);
        this.gameRule = this.availableGameRules.get();
        this.currentGame = this.createNewGame(this.gameRule);

        setTimeout(() => {
            this.setState({ stage: 2 });
        },20);
    }

    startNew() {
        this.gameRule = null;
        this.gameSize = null;
        this.gameRange = null;
        this.setState({stage: 0});

    }

    createNewGame(gameRule) {
        let gameGenerator = GameGenerator();
        gameGenerator.setRules(gameRule);
        return gameGenerator.newGame();
    }

    getSizeOptions() {
        if (this.isScreen('selectSize')) {
            const gameSizes = Object.keys(this.gameSizes);
            const handleClick = (event) => {
                this.gameSize = this.gameSizes[event.target.dataset.id];
                this.prepareGame();
            };

            return (
                <section className="start-game">
                    {gameSizes.map(item => (
                        <React.Fragment key={item}>
                            <button type="button" onClick={handleClick} data-id={item}>{item}</button>
                        </React.Fragment>
                    ))}
                </section>
            )
        }
    }

    getRangeScreen() {
        if (this.isScreen('selectRange')) {
            const gameModes = Object.keys(this.gameModes);
            const handleClick = (event) => {
                this.gameRange = this.gameModes[event.target.dataset.id];
                this.setState({ stage: 1 });
            };

            return (
                <section className="start-game">
                {gameModes.map(item  => (
                    <React.Fragment key={item}>
                        <button type="button" onClick={handleClick} data-id={item}>{item}</button>
                    </React.Fragment>
                ))}
                 </section>
            )
        }
    }
    getHeader() {
        return (
            <header>
                <h3>Rullo Remake (with React.js)</h3>
            </header>
        );
    }
    getGameBoard() {
        if (this.isScreen('startGame')) {
            return (
                <Board currentGame={this.currentGame} onQuit={this.startNew}/>
            )
        }
    }

    render() {
        return (
            <div className="container mx-auto px-4 py-5">
                <div className="rullo-game">
                    {this.getHeader()}
                    {this.getRangeScreen()}
                    {this.getSizeOptions()}
                    {this.getGameBoard()}
                </div>
            </div>
        )
    }
}

export default RulloGame;