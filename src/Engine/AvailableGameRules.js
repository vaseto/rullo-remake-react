import GameRule from "./GameRule";

/**
 * @return {{
 *      ranges: *,
 *      sizes: *,
 *      selectRange: (function(*, *): {min, max, x, y})
 * }}
 * @constructor
 */
/**
 *
 * @return {{
 *      ranges: string[],
 *      sizes: string[],
 *      setSize: setSize,
 *      setRange: setRange,
 *      get: (function(): {min, max, x, y})
 * }}
 * @constructor
 */
const AvailableGameRules = () => {
    const rangeIds = ['0-9', '0-19', '2-4'];
    const ranges = {
        '0-9':  {min: 1, max: 9},
        '0-19': {min: 1, max: 19},
        '2-4':  {min: 2, max: 4}
    }

    const sizeIds = ['5x5', '6x6', '7x7', '8x8'];
    const sizes = {
        '5x5': {x: 5, y: 5},
        '6x6': {x: 6, y: 6},
        '7x7': {x: 7, y: 7},
        '8x8': {x: 8, y: 8},
    };

    let min, max, x, y;

    const setRange = (rangeId) => {
        if (ranges.hasOwnProperty(rangeId)) {
            min = ranges[rangeId].min;
            max = ranges[rangeId].max;
        }
    }

    const setSize = (sizeId) => {
        if (sizes.hasOwnProperty(sizeId)) {
            x = sizes[sizeId].x;
            y = sizes[sizeId].y
        }
    }

    const get = () => {
        return GameRule(x, y, max, min);
    }

    return {
        ranges: rangeIds,
        sizes: sizeIds,
        setRange: setRange,
        setSize: setSize,
        get: get,
    }
}

export default AvailableGameRules;