/**
 *
 * @param {array} rowTargets
 * @param {array} columnTargets
 * @param {array} cellMatrix
 * @return {{
 *       cells,
 *       columnTargets,
 *       rowTargets
 *  }}
 * @constructor
 */
const GameSettings = (rowTargets, columnTargets, cellMatrix) => {
    return {
        rowTargets: rowTargets,
        columnTargets: columnTargets,
        cells: cellMatrix
    }
}

export default GameSettings;