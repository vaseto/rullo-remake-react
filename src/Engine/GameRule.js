/**
 * Most basic game rule definition
 * @param _x
 * @param _y
 * @param _max
 * @param _min
 * @return {{min, max, x, y}}
 * @constructor
 */
const GameRule = (_x, _y, _max, _min) => {
    return {
        x: _x,
        y: _y,
        min: _min,
        max: _max
    }
}

export default GameRule;