import React from "react"

class Board extends React.Component {
    constructor(props) {
        super(props);

        this.cells = props.currentGame.cells;
        this.rowTargets = props.currentGame.rowTargets;
        this.columnTargets = props.currentGame.columnTargets;

        this.currentGame = props.currentGame;
        this.onQuit = props.onQuit;
    }

    eachRow(rowItem) {
        const currentRow = this.cells[rowItem];
        const columnKeys = Object.keys(currentRow);

        return (columnKeys.map(columnItem =>
            <React.Fragment key={columnItem}>
                <td className="board-cell">
                    <div data-column={columnItem}
                        data-row={rowItem}
                        data-value={currentRow[columnItem]}
                    >{currentRow[columnItem]}</div>
                    </td>
            </React.Fragment>
            )
        )
    }

    renderRows() {
        const rowKeys = Object.keys(this.cells);

        return (rowKeys.map(rowItem =>
            <React.Fragment key={rowItem}>
                <tr className="board-row">
                    { this.eachRow(rowItem) }
                    { this.renderRowTarget(rowItem) }
                </tr>
            </React.Fragment>
            )
        )
    }

    renderRowTarget(rowItem) {
        return (
            <td className='board-target' data-row={rowItem}>{this.rowTargets[rowItem]}</td>
        )
    }

    renderColumnTargets() {
        const columnKeys = Object.keys(this.columnTargets);
        return (
            <tr className="board-row">
            {columnKeys.map(columnItem =>
                <React.Fragment key={columnItem}>
                    <th className='board-target' data-column={columnItem}>{this.columnTargets[columnItem]}</th>
                </React.Fragment>
            )}
            </tr>
        )
    }

    render() {
        return (
            <table className="rullo-board">
                <tbody>
                    {this.renderColumnTargets()}
                    {this.renderRows()}
                </tbody>
            </table>
        );
    }
}

export default Board;