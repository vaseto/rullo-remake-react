import GameRule from "./GameRule";
import GameSettings from "./GameSettings";

const {rando} = require('@nastyox/rando.js');

/**
 *
 * @return {{
 *      newGame: (function(): {cells, columnTargets, rowTargets}),
 *      setRules: {GameRule}
 *  }}
 * @constructor
 */
function GameGenerator () {
    let rows, columns, cellMin, cellMax;

    const generateCellValue = () => {
        // Select a value between min and max
        return rando(cellMin, cellMax);
    }

    const getEmptyTarget = (maxValue) => {
        let target = [];
        for (let i = 0; i < maxValue; i++) {
            target[i] = 0;
        }
        return target;
    }

    const selectCellForTargets = () =>  {
        return Math.round(rando() * 1000) > 500;
    }

    const newGame = () => {
        // Generate the initial matrix
        let gameMatrix = [],
            targetRow = getEmptyTarget(rows),
            targetColumn = getEmptyTarget(columns),
            i, j;

        for (i = 0; i < rows; i++) {
            gameMatrix[i] = [];
            for (j = 0; j < columns; j++) {
                const cell = generateCellValue();
                if (selectCellForTargets()) {
                    targetRow[i] += cell;
                    targetColumn[j] += cell;
                }

                gameMatrix[i][j] = cell;
            }
        }

        return GameSettings(targetRow, targetColumn, gameMatrix);
    }

    return {
        /**
         * @var {{ GameRule }} gameRule
         */
        setRules: (GameRule) => {
            // Game parameters
            rows   = GameRule.x;
            columns = GameRule.y;
            cellMax = GameRule.max;
            cellMin = GameRule.min;
        },
        newGame: newGame,
    }
}

export default GameGenerator;